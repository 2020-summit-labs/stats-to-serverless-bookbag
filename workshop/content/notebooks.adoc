:USER_GUID: %guid%
:USERNAME: %user%
:markup-in-source: verbatim,attributes,quotes
:show_solution: true


The notebooks you'll work through today are numbered in order and (for the most part) you'll need to run them in order. Here's how to step through the notebooks:


- `01-eda.ipynb` steps through some exploratory data analysis, allowing you to gain insight into the transactions data you will be working with today. You'll also inspect and transform some of the data.
- `02-feature-engineering.ipynb` shows how to turn the transactions data into feature vectors. You'll transform each entry in the data set into numeric vector, whilst aiming to capture important information about the data. You will visualise these vectors to indicate if it is possible to identify any structure in the data that differentiates the legitimate transactions from the fraudulent ones.

**At this point in the workshop you can choose which type of model you train.** 
We have a `logistic regression model` and a `random forest model` for you to choose from. 

- `03-model-logistic-regression.ipynb` shows how to train a logistic regression model to distinguish between fraudulent and legitimate transactions; and
- `03-model-random-forest.ipynb` shows how to train an ensemble of decision trees to distinguish between fraudulent and legitimate transactions.

If you want, you can train both of the models, but whichever model you trained last will be read in by the next notebook. 

Congratulations! You've now stepped through training a model. 


We will now use OpenShift to deploy this model as a service, but before we can do this we need to log into into the OpenShift Console. 
